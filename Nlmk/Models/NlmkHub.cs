﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using Quartz;

namespace Nlmk.Models
{
    public class NlmkHub : Hub, IJob
    {
        static int val = 124;
        static int minValue = 123;
        static bool error = false;

        static string mail = "seruyworon15@gmail.com";
        static bool isMail = true;
        static string phone = "79513009712";
        static bool isPhone = false;
        public int ValRandom { get=> val; set { val = value; } }
        public string Mail { get => mail; set { mail = value; } }
        public bool IsMail { get => isMail; set { isMail = value; } }
        public string Phone { get => phone; set { phone = value; } }
        public bool IsPhone { get => isPhone; set { isPhone = value; } }
        public int MinValue { get => minValue; set { minValue = value; } }
        static TimeSpan timeEvent;

        static List<Zinced> zinceds;
        IHubContext hub = GlobalHost.ConnectionManager.GetHubContext<NlmkHub>();
     
        public async Task Execute(IJobExecutionContext context)
        {
            GetValue();
        }

        public void GetValue()
        {
            var rnd = new Random();
            var date = DateTime.Now;
            double dRandValue=0;
            do
            {
                double dSumm = 0;
                for (int i = 0; i <= 12; i++)
                {
                    double R = rnd.NextDouble();
                    dSumm = dSumm + R;
                }
                dRandValue = Math.Round((-0.5 + 0.5 * (dSumm - 6)), 3);
            } while (dRandValue > 1 || dRandValue < -1);

            var zinced = new Zinced(date, val+rnd.Next(-3,3)* dRandValue);
            zinceds.Add(zinced);
            zinceds.RemoveAt(0);
            hub.Clients.All.graphvalue(zinced);
            if (zinceds.Where(x => x.Date >= date.AddMinutes(-2)).Select(x => x.Value).Average() < minValue )
            {
                if (!error) {
                error = true;
                    var message = $"На АНГЦ зафиксировано падение скорости в {DateTime.Now.ToShortTimeString().ToString()}, график http://localhost:5443/";
                    if (isMail)  new SendEmail().Send(mail, message);
                    if(isPhone)  new SendSms().Send(phone, message);
                    timeEvent = DateTime.Now.TimeOfDay;

                }
            }
            else {
                if (error) 
                {
                    error = false;
                    var message = $"АНГЦ: скорость восстановлена {DateTime.Now.ToShortTimeString().ToString()}, длительность отклонения {DateTime.Now.TimeOfDay.Subtract(timeEvent)}, график http://localhost:5443/";
                    if (isMail) new SendEmail().Send(mail, message);
                    if (isPhone) new SendSms().Send(phone, message);
                }
                error = false; }
        }

        public void Servise()
        {
           
            if (zinceds is null) 
            {
                zinceds = new List<Zinced>();
                var date = DateTime.Now;
                var rnd = new Random();
                for (var i = date.AddMinutes(-20); i < date; i = i.AddSeconds(1))
                    zinceds.Add(new Zinced(i, (double)rnd.Next((int)(val * 10 - val * 10 * 0.01), (int)(val * 10 + val * 10 * 0.02)) / 10));
            }
            try
            {
                Clients.Caller.graph(zinceds);
            }
            catch { }
            
        }
        
    }
}