﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nlmk.Models
{
    public class Zinced
    {
        public Zinced(DateTime date, double value) 
        {
            Date = date;
            Value = value;
        }
        public DateTime Date { get; set; }
        public double Value { get; set; }
        public string DateString { get => Date.ToString("yyyy-MM-ddTHH:mm:ssZ"); }
    }
}