﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nlmk.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public void SetValue(int Random, int Min, bool IsMail, string Mail, bool IsPhone, string Phone) 
        {
            var res = new Nlmk.Models.NlmkHub();
            res.ValRandom = Math.Max(Random,0);
            res.MinValue = Min;
            res.IsMail = IsMail;
            res.Mail = Mail;
            res.IsPhone = IsPhone;
            res.Phone = Phone;
        }

      
    }
}